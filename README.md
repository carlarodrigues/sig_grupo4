# SIG_Grupo4

<h4>Plugin para extração dos dados mundiais da vacinação COVID-19, e a sua representação temporal no QGIS.</h4>

**Coronavirus (COVID-19) Vaccinations**:
https://ourworldindata.org/covid-vaccinations?fbclid=IwAR2lJXBCRlx7BPhnE1__VLsrSTE_Dwmw-iUYWfYwBk5d1eveZoK2ZXWo8UE

**Temporal Data QGIS**:
- https://www.qgistutorials.com/en/docs/3/animating_time_series.html
- https://www.birdseyeviewgis.com/blog/2020/8/14/creating-a-covid-19-temporal-animation-with-qgis

**Plugin Tutorial**:
- https://www.qgistutorials.com/en/docs/3/building_a_python_plugin.html
- https://www.qgistutorials.com/en/docs/3/processing_python_plugin.html



